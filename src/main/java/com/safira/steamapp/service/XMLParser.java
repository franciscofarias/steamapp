package com.safira.steamapp.service;


import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.GameNew;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.exceptions.SteamVanityURLNoMatchException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco Farías on 21/01/15.
 */
public class XMLParser {

    public String getSteamID64(String xml) throws SteamVanityURLNoMatchException, JDOMException {
        SAXBuilder saxBuilder = new SAXBuilder();
        try {
            Document document = saxBuilder.build(new StringReader(xml));
            Element rootNode = document.getRootElement();
            if ((rootNode.getChildText("success")).equals("42")) throw new SteamVanityURLNoMatchException();
            return rootNode.getChildText("steamid");
        } catch (IOException e) {
            return "ERROR";
        }
    }

    public List<Game> retrieveGames(String xml) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        List<Game> gameNameList = new ArrayList<>();
        Game game;
        Document document = saxBuilder.build(new StringReader(xml));
        Element rootNode = document.getRootElement();
        List<Element> subnode = rootNode.getChildren("games");
        for (Element el : subnode) {
            List<Element> list = el.getChildren("message");
            for (Element element : list) {
                game = new Game(
                        element.getChildText("appid"),
                        element.getChildText("name"),
                        element.getChildText("playtime_forever"),
                        element.getChildText("img_icon_url"),
                        element.getChildText("img_logo_url")
                );
                gameNameList.add(game);
            }
        }
        return gameNameList;
    }

    public String retrieveFriendList(String xml) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        String steamID64List = "";
        Document document = saxBuilder.build(new StringReader(xml));
        Element rootNode = document.getRootElement();
        List<Element> subnode = rootNode.getChildren("friends");
        for (Element el : subnode) {
            List<Element> list = el.getChildren("friend");
            for (Element element : list) {
                steamID64List += element.getChildText("steamid") + ",";
            }
        }
        if (steamID64List.length() > 0) {
            return steamID64List.substring(0, steamID64List.length() - 1);
        }
        return null;
    }

    public List<Player> retrievePlayerSummaries(String xml) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        List<Player> playerList = null;
        Player player;
        Document document = saxBuilder.build(new StringReader(xml));
        Element rootNode = document.getRootElement();
        List<Element> subnode = rootNode.getChildren("players");
        if (subnode != null) {
            playerList = new ArrayList<>();
            for (Element el : subnode) {
                List<Element> list = el.getChildren("player");
                for (Element element : list) {
                    player = new Player(
                            element.getChildText("steamid"),
                            element.getChildText("personaname"),
                            element.getChildText("profileurl"),
                            element.getChildText("avatarfull"),
                            element.getChildText("realname")
                    );
                    playerList.add(player);
                }
            }
        }
        return playerList;
    }

    public Player retrieveUserSummary(String xml) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        Player player = null;
        Document document = saxBuilder.build(new StringReader(xml));
        Element rootNode = document.getRootElement();
        List<Element> subnode = rootNode.getChildren("players");
        for (Element el : subnode) {
            List<Element> list = el.getChildren("player");
            for (Element element : list) {
                player = new Player(
                        element.getChildText("steamid"),
                        element.getChildText("personaname"),
                        element.getChildText("profileurl"),
                        element.getChildText("avatarfull"),
                        element.getChildText("realname")
                );
            }
        }
        return player;
    }

    public List<GameNew> retrieveGameNews(String xml) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        List<GameNew> gameNewList = null;
        GameNew gameNew;
        Document document = saxBuilder.build(new StringReader(xml));
        Element rootNode = document.getRootElement();
        List<Element> subnode = rootNode.getChildren("newsitems");
        if (subnode != null) {
            gameNewList = new ArrayList<>();
            for (Element el : subnode) {
                List<Element> elementList = el.getChildren("newsitem");
                for (Element newitem : elementList) {
                    gameNew = new GameNew(
                            newitem.getChildText("title"),
                            newitem.getChildText("url"),
                            newitem.getChildText("author"),
                            newitem.getChildText("contents"),
                            newitem.getChildText("date")
                    );
                    gameNewList.add(gameNew);
                }
            }

        }
        return gameNewList;
    }
}
