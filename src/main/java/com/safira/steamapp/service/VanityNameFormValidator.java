package com.safira.steamapp.service;

import com.safira.steamapp.domain.SteamError;
import com.safira.steamapp.domain.VanityNameForm;
import org.springframework.validation.BindingResult;

/**
 * Created by developer on 30/01/15.
 */
public class VanityNameFormValidator {
    public static void  validateForm(VanityNameForm vanityNameForm, BindingResult results){
        if(vanityNameForm.getNickname().length() < 2 ||
                vanityNameForm.getNickname().length() > 32) {
            results.addError(new SteamError("vanity.name.length.error"));
        }
    }
}
