package com.safira.steamapp.service;

import com.safira.steamapp.common.Key;
import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.GameNew;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import org.jdom2.JDOMException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Francisco Farías on 21/01/15.
 */

public class HTTPRequester {

    private final String USER_AGENT = "Mozilla/5.0";

    public String getOwnedGames(String steamID64) throws IOException, SteamAPIBadRequestException {
        String url = keyAppender(URLs.GET_OWNED_GAMES);
        url = url.replaceAll(URLs.STEAM_ID_64, steamID64);
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return response;
    }

    public String getSteamID64(String vanity) throws IOException, SteamAPIBadRequestException {
        String url = keyAppender(URLs.RESOLVE_VANITY_URL);
        url = url.replaceAll(URLs.VANITY_URL, vanity);
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return response;
    }

    public String getImage(String hash, String appID) {
        String url = URLs.IMAGE_CONSTRUCT_URL;
        url = url.replaceAll(URLs.HASH, hash);
        url = url.replaceAll(URLs.APP_ID, appID);
        return url;
    }

    public List<Player> retrieveFriendListAndInfo(String steamID64) throws IOException, SteamAPIBadRequestException, JDOMException {
        String friendListXML = getFriendList(steamID64);
        XMLParser xml = new XMLParser();
        String friendSteamID64List = xml.retrieveFriendList(friendListXML);
        if (friendSteamID64List != null) {
            String friendSummariesXML = getPlayerSummaries(friendSteamID64List);
            return xml.retrievePlayerSummaries(friendSummariesXML);
        } else {
            return new ArrayList<Player>();
        }
    }

    private String getFriendList(String steamID64) throws IOException, SteamAPIBadRequestException {
        String url = keyAppender(URLs.GET_FRIEND_LIST);
        url = url.replaceAll(URLs.STEAM_ID_64, steamID64);
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return response;
    }

    private String getPlayerSummaries(String steamID64s) throws IOException, SteamAPIBadRequestException {
        String url = keyAppender(URLs.GET_PLAYER_SUMMARIES);
        url = url.replaceAll(URLs.STEAM_ID_64, steamID64s);
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return response;
    }

    public Player getUserSummary(String steamID64) throws IOException, SteamAPIBadRequestException, JDOMException {
        String url = keyAppender(URLs.GET_PLAYER_SUMMARIES);
        url = url.replaceAll(URLs.STEAM_ID_64, steamID64);
        XMLParser xml = new XMLParser();
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return xml.retrieveUserSummary(sendGet(url));
    }

    public List<GameNew> getGameNews(String appID) throws IOException, SteamAPIBadRequestException, JDOMException {
        String url = URLs.GET_GAME_NEWS;
        url = url.replaceAll(URLs.APP_ID, appID);
        XMLParser xml = new XMLParser();
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        return xml.retrieveGameNews(response);
    }

    public List<Game> getRecentlyPlayedGames(String steamID64) throws IOException, SteamAPIBadRequestException, JDOMException {
        String url = keyAppender(URLs.GET_RECENTLY_PLAYED_GAMES);
        url = url.replaceAll(URLs.STEAM_ID_64, steamID64);
        String response = sendGet(url);
        if (response == null) throw new SteamAPIBadRequestException();
        XMLParser xml = new XMLParser();
        return xml.retrieveGames(response);
    }

    private String keyAppender(String url) {
        return url.replaceAll(URLs.KEY, Key.KEY);
    }

    private String sendGet(String url) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
}
