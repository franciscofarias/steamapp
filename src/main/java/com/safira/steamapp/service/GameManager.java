package com.safira.steamapp.service;

import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.repository.GameDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by developer on 02/10/15.
 */
@Component
public class GameManager {

    @Autowired
    private GameDao gameDao;

    public void setGameDao (GameDao gameDao) {
        this.gameDao = gameDao;
    }

    public List<Game> getGames() {
        return gameDao.getGameList();
    }

    public void addGame(Game game) {
        gameDao.addGame(game);
    }

    public void updateGame(Game game) {
        gameDao.saveGame(game);
    }

    public List<Game> getGamesByPlayer(long playerId) {
        return gameDao.getGamesByOwner(playerId);
    }

    public Game getGameByOwnerAndAppId(Player player, String appId) {
        return gameDao.getGameByOwnerAndAppId(player, appId);
    }

    public void deleteGame(Game game) {
        gameDao.deleteGame(game);
    }

}
