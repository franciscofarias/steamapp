package com.safira.steamapp.service;

import com.safira.steamapp.domain.Player;
import com.safira.steamapp.repository.PlayerDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by developer on 30/09/15.
 */
@Component
public class PlayerManager {

    @Autowired
    private PlayerDao playerDao;

    public void setPlayerDao(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    public List<Player> getPlayers() {
        return playerDao.getPlayerList();
    }

    public void addPlayer(Player player) {
        playerDao.addPlayer(player);
    }

    public void updatePlayer(Player player) {
        playerDao.savePlayer(player);
    }

    public Player getPlayerById(long id) {
        return playerDao.getPlayerById(id);
    }

    public Player getPlayerBySteamId(String steamId64) {
        return playerDao.getPlayerBySteamId(steamId64);
    }

}
