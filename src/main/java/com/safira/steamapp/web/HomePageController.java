package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.domain.SteamError;
import com.safira.steamapp.domain.SteamUser;
import com.safira.steamapp.domain.VanityNameForm;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import com.safira.steamapp.exceptions.SteamVanityURLNoMatchException;
import com.safira.steamapp.service.HTTPRequester;
import com.safira.steamapp.service.PlayerManager;
import com.safira.steamapp.service.VanityNameFormValidator;
import com.safira.steamapp.service.XMLParser;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Francisco Farías on 21/01/15.
 */

@Controller
public class HomePageController {

    @Autowired
    private PlayerManager playerManager;

    public void setPlayerManager(PlayerManager playerManager) {
        this.playerManager = playerManager;
    }

    @ModelAttribute("vanityNameForm")
    public VanityNameForm addFormToPage() {
        return new VanityNameForm();
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String home(HttpServletRequest request, Model model) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");
        if (steamUser == null) {
            return "home";
        } else {
            model.addAttribute("vanity", steamUser.getVanity());
            return URLs.USER;
        }

    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.POST)
    public String home(@ModelAttribute("vanityNameForm") VanityNameForm vanityNameForm,
                       BindingResult results,
                       HttpServletRequest request,
                       Model model) {
        VanityNameFormValidator.validateForm(vanityNameForm, results);
        HTTPRequester http = new HTTPRequester();
        try {
            String response = http.getSteamID64(vanityNameForm.getNickname());
            XMLParser xmlParser = new XMLParser();
            String steamID64 = xmlParser.getSteamID64(response);
            Player dbPlayer = this.playerManager.getPlayerBySteamId(steamID64);
            if (dbPlayer == null) {
                dbPlayer = http.getUserSummary(steamID64);
                this.playerManager.addPlayer(dbPlayer);
            }
            SteamUser steamUser = new SteamUser(steamID64, vanityNameForm.getNickname(), dbPlayer);
            request.getSession().setAttribute("steamUser", steamUser);
        } catch (SteamVanityURLNoMatchException e) {
            results.addError(new SteamError("vanity.url.match.error"));
            e.printStackTrace();
        } catch (IOException e) {
            results.addError(new SteamError("io.error"));
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            results.addError(new SteamError("api.bad.request.error"));
            e.printStackTrace();
        } catch (JDOMException e) {
            results.addError(new SteamError("jdom.error"));
            e.printStackTrace();
        }
        if(results.hasErrors()){
            model.addAttribute(BindingResult.MODEL_KEY_PREFIX + "vanityNameForm", results);
            return URLs.HOME;
        }
        model.addAttribute("vanity", vanityNameForm.getNickname());
        return URLs.USER;
    }

    @RequestMapping(value = "/quit")
    public String quit(HttpServletRequest request) {

        HttpSession session = request.getSession(false);
        if (session != null) session.invalidate();
        return "redirect:/";

    }

    @RequestMapping(value = "/updateData")
    public String updateData(HttpServletRequest request) {
        Player actualPlayer = ((SteamUser) request.getSession().getAttribute("steamUser")).getUser();
        HTTPRequester http = new HTTPRequester();
        try {
            Player updatedPlayer = http.getUserSummary(actualPlayer.getSteamID64());
            updatedPlayer.setId(actualPlayer.getId());
            this.playerManager.updatePlayer(updatedPlayer);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            e.printStackTrace();
        } catch (JDOMException e) {
            e.printStackTrace();
        }
        return "redirect:/home";
    }

}