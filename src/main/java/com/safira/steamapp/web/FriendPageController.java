package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.domain.SteamUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Francisco Farías on 22/01/15.
 */
@Controller
public class FriendPageController {

    @RequestMapping(value = "/friend", method = RequestMethod.GET)
    public String friend(HttpServletRequest request, Model model, @RequestParam(value = "steamID64", required = true) String steamID64){

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        List<Player> friendList = ((List<Player>) request.getSession().getAttribute("friendList"));
        for(Player player : friendList)
            if (player.getSteamID64().equals(steamID64)) {
                model.addAttribute("friend", player);
                break;
            }
        return URLs.FRIEND;
    }
}
