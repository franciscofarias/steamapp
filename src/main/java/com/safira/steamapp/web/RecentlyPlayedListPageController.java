package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.SteamUser;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import com.safira.steamapp.service.HTTPRequester;
import org.jdom2.JDOMException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by developer on 28/09/15.
 */
@Controller

public class RecentlyPlayedListPageController {

    @RequestMapping(value = "/recentlyPlayed", method = RequestMethod.GET)
    public String recentlyPlayedList(HttpServletRequest request, Model model, @RequestParam(value = "steamID64", required = false) String steamID) {
        HTTPRequester http = new HTTPRequester();
        List<Game> gameList = null;

        if (steamID == null) {
            SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");
            if (steamUser != null) {
                steamID = steamUser.getSteamID64();
            } else {
                return "redirect:/";
            }
        }
        try {
            gameList = http.getRecentlyPlayedGames(steamID);
            request.getSession().setAttribute("gameList", gameList);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        model.addAttribute("games",gameList);
        return URLs.RECENTLY_PLAYED;
    }
}
