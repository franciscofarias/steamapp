package com.safira.steamapp.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Francisco Farías on 21/01/15.
 */
@Controller
public class UserPageController {

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public String user(HttpServletRequest request, Model model) {
        model.addAttribute("steamUser", (request.getSession().getAttribute("steamUser")));
        return "user";
    }
}
