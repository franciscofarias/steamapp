package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.GameNew;
import com.safira.steamapp.domain.SteamError;
import com.safira.steamapp.domain.SteamUser;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import com.safira.steamapp.service.HTTPRequester;
import org.jdom2.JDOMException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by developer on 25/09/15.
 */

@Controller
public class GameNewsPageController {
    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String news(HttpServletRequest request, Model model, @RequestParam(value = "appid", required = true) String appid) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        HTTPRequester http = new HTTPRequester();
        List<GameNew> news = null;
        try {
            news = http.getGameNews(appid);
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        model.addAttribute("news", news);
        return URLs.NEWS;
    }

}
