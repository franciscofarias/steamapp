package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.domain.SteamUser;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import com.safira.steamapp.service.GameManager;
import com.safira.steamapp.service.HTTPRequester;
import com.safira.steamapp.service.XMLParser;
import org.jdom2.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Francisco Farías on 21/01/15.
 */
@Controller
public class GameListPageController {

    @Autowired
    private GameManager gameManager;

    public void setGameManager (GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @RequestMapping(value = "/gameList", method = RequestMethod.GET)
    public String gameList(HttpServletRequest request, Model model) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        String response;
        List<Game> gameList = new ArrayList<>();
        try {
            Player player = steamUser.getUser();
            gameList = gameManager.getGamesByPlayer(player.getId());
            if (gameList.size() == 0) {
                XMLParser xmlParser = new XMLParser();
                HTTPRequester http = new HTTPRequester();
                String steamID64 = player.getSteamID64();
                response = http.getOwnedGames(steamID64);
                gameList = xmlParser.retrieveGames(response);

                for (Game game : gameList) {
                    game.setOwner(player);
                    gameManager.addGame(game);
                }
            }
            request.getSession().setAttribute("gameList", gameList);
        } catch (IOException e) {
            //TODO
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            //TODO
            e.printStackTrace();
        } catch (JDOMException e) {
            //TODO
            e.printStackTrace();
        }
        Collections.sort(gameList, Game.GameNameComparator);
        model.addAttribute("gameList", gameList);
        return URLs.GAME_LIST;
    }

    @RequestMapping(value = "/updateGameData", method = RequestMethod.GET)
    public String updateGameData(HttpServletRequest request, Model model) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        Player actualPlayer = steamUser.getUser();
        List<Game> gameList = new ArrayList<>();
        /*gameList = gameManager.getGamesByPlayer(actualPlayer.getId());

        // limpiar todos los games
        for (Game game : gameList) {
            gameManager.deleteGame(game);
        }*/

        try {
            XMLParser xmlParser = new XMLParser();
            HTTPRequester http = new HTTPRequester();
            String steamID64 = actualPlayer.getSteamID64();
            String response = null;
            response = http.getOwnedGames(steamID64);
            gameList = xmlParser.retrieveGames(response);

            // agregar los juegos actuales
            for (Game game : gameList) {
                Game dbGame = gameManager.getGameByOwnerAndAppId(actualPlayer,game.getAppid());

                if (dbGame != null) {
                    dbGame.setPlayTime(game.getPlayTime());
                    gameManager.updateGame(dbGame);
                } else {
                    gameManager.addGame(game);
                }
            }

        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            e.printStackTrace();
        }
        Collections.sort(gameList, Game.GameNameComparator);
        model.addAttribute("gameList", gameList);
        return URLs.GAME_LIST;
    }
}