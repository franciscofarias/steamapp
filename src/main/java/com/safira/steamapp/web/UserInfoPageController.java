package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.SteamUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Francisco Farías on 22/01/15.
 */
@Controller
public class UserInfoPageController {

    @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
    public String userInfo(HttpServletRequest request, Model model) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        model.addAttribute("steamUser", (request.getSession().getAttribute("steamUser")));
        return URLs.USER_INFO;
    }
}
