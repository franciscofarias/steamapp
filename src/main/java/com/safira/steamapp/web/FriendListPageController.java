package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Player;
import com.safira.steamapp.domain.SteamUser;
import com.safira.steamapp.exceptions.SteamAPIBadRequestException;
import com.safira.steamapp.service.HTTPRequester;
import org.jdom2.JDOMException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Francisco Farías on 22/01/15.
 */
@Controller

public class FriendListPageController {

    @RequestMapping(value = "/friendList", method = RequestMethod.GET)
    public String friendlist(HttpServletRequest request, Model model){

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        HTTPRequester http = new HTTPRequester();
        List<Player> friendList = null;
        try {
            String steamID64 = ((SteamUser) request.getSession().getAttribute("steamUser")).getSteamID64();
            friendList = http.retrieveFriendListAndInfo(steamID64);
        } catch (IOException e) {
            //TODO
            e.printStackTrace();
        } catch (SteamAPIBadRequestException e) {
            //TODO
            e.printStackTrace();
        } catch (JDOMException e) {
            //TODO
            e.printStackTrace();
        }
        request.getSession().setAttribute("friendList", friendList);
        if(friendList == null){
            model.addAttribute("friends", new ArrayList<Player>());
        }
        Collections.sort(friendList, Player.PlayerPersonalNameComparator);
        model.addAttribute("friends", friendList);
        return URLs.FRIEND_LIST;
    }
}
