package com.safira.steamapp.web;

import com.safira.steamapp.common.URLs;
import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.SteamUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Francisco Farías on 21/01/15.
 */
@Controller
public class GamePageController {

    @RequestMapping(value = "/game", method = RequestMethod.GET)
    public String game(HttpServletRequest request, Model model, @RequestParam(value = "appid", required = false) String appid) {

        SteamUser steamUser = (SteamUser) request.getSession().getAttribute("steamUser");

        if (steamUser == null) return "redirect:/";

        List<Game> gameList = (List<Game>) request.getSession().getAttribute("gameList");
        for(Game game : gameList){
            if(appid.equals(game.getAppid())){
                model.addAttribute("game", game);
            }
        }
        return URLs.GAME;
    }

}
