package com.safira.steamapp.exceptions;

/**
 * Created by developer on 23/01/15.
 */
public class SteamVanityURLNoMatchException extends Exception {

    public SteamVanityURLNoMatchException(){}

    public SteamVanityURLNoMatchException(String message){
        super(message);
    }

    public SteamVanityURLNoMatchException(Throwable cause) {
        super (cause);
    }

    public SteamVanityURLNoMatchException(String message, Throwable cause) {
        super (message, cause);
    }
}
