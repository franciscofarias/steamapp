package com.safira.steamapp.exceptions;

/**
 * Created by developer on 22/01/15.
 */
public class SteamAPIBadRequestException extends Exception {

    public SteamAPIBadRequestException(){}

    public SteamAPIBadRequestException(String message){
        super(message);
    }

    public SteamAPIBadRequestException(Throwable cause) {
        super (cause);
    }

    public SteamAPIBadRequestException(String message, Throwable cause) {
        super (message, cause);
    }
}
