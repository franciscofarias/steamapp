package com.safira.steamapp.common;

/**
 * Created by Francisco Farías on 21/01/15.
 */
public class URLs {
    public static final String APP_ID = "<--APP_ID-->";
    public static final String KEY = "<--KEY-->";
    public static final String STEAM_ID_64 = "<--STEAM_ID_64-->";
    public static final String VANITY_URL = "<--VANITY_URL-->";
    public static final String HASH = "<--HASH-->";

    public static final String GET_FRIEND_LIST = "http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key="+KEY+"&steamid="+ STEAM_ID_64 +"&relationship=friend&format=xml";
    public static final String GET_PLAYER_SUMMARIES = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key="+KEY+"&steamids="+ STEAM_ID_64 +"&format=xml";
    public static final String GET_OWNED_GAMES = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key="+KEY+"&steamid="+ STEAM_ID_64 +"&format=xml&include_appinfo=1&include_played_free_games=1";
    public static final String GET_GAME_NEWS = " http://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid="+APP_ID+"&count=3&maxlength=300&format=xml";
    public static final String GET_RECENTLY_PLAYED_GAMES = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key="+KEY+"&steamid="+STEAM_ID_64+"&format=xml";
    public static final String RESOLVE_VANITY_URL = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key="+KEY+"&vanityurl="+ VANITY_URL +"&format=xml";
    public static final String IMAGE_CONSTRUCT_URL ="http://media.steampowered.com/steamcommunity/public/images/apps/"+ APP_ID +"/"+HASH+".jpg";

    public static final String HOME = "home";
    public static final String USER = "user";
    public static final String USER_INFO = "userInfo";
    public static final String FRIEND = "friend";
    public static final String FRIEND_LIST = "friendList";
    public static final String GAME_LIST = "gameList";
    public static final String GAME = "game";
    public static final String NEWS = "news";
    public static final String RECENTLY_PLAYED = "recentlyPlayed";
}
