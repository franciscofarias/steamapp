package com.safira.steamapp.domain;


/**
 * Created by developer on 23/01/15.
 */
public class VanityNameForm {

    private String nickname;

    public VanityNameForm(){
    }

    public VanityNameForm(String nickname){
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String toString() {
        return "Vanity Name(Nickname: " + this.nickname + ")";
    }
}
