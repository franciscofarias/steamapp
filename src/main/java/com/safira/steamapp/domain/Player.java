package com.safira.steamapp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Francisco Farías on 22/01/15.
 */

@Entity
@Table(name = "PLAYERS")
public class Player implements Comparable<Player>, Serializable {
    
    @Id
    @Column(name="PLAYER_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="PLAYER_STEAM_ID")
    private String steamID64;
    
    @Column(name="PLAYER_PERSONAL_NAME")
    private String personalName;
    
    @Column(name="PLAYER_PROFILE_URL")
    private String profileURL;
    
    @Column(name="PLAYER_AVATAR_FULL_URL")
    private String avatarFullURL;
    
    @Column(name="PLAYER_REAL_NAME")
    private String realName;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<Game> ownedGames;

    public List<Game> getOwnedGames() {
        return ownedGames;
    }

    public void setOwnedGames(List<Game> ownedGames) {
        this.ownedGames = ownedGames;
    }
    //Default constructor

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Player() {}

    public Player(String steamID64, String personalName, String profileURL, String avatarFullURL, String realName){
        this.steamID64 = steamID64;
        this.personalName = personalName;
        this.profileURL = profileURL;
        this.avatarFullURL = avatarFullURL;
        this.realName = realName;
    }

    public void setSteamID64(String steamID64) {
        this.steamID64 = steamID64;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }

    public void setAvatarFullURL(String avatarFullURL) {
        this.avatarFullURL = avatarFullURL;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSteamID64() {
        return steamID64;
    }

    public String getPersonalName() {
        return personalName;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public String getAvatarFullURL() {
        return avatarFullURL;
    }

    public String getRealName() {
        return realName;
    }

    @Override
    public int compareTo(Player o) {
        return 0;
    }


    public static Comparator<Player> PlayerPersonalNameComparator = new Comparator<Player>() {
        public int compare(Player player1, Player player2) {
            String playerName1 = player1.getPersonalName().toUpperCase();
            String playerName2 = player2.getPersonalName().toUpperCase();

            return playerName1.compareTo(playerName2);
        }
    };

}
