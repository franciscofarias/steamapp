package com.safira.steamapp.domain;

import org.springframework.validation.ObjectError;

/**
 * Created by Francisco Farías on 26/01/15.
 */
public class SteamError extends ObjectError {
    private String code;


    public SteamError(String code) {
        super(code, "error");
        this.code = code;
    }

    public SteamError(String code, String defaultMessage) {
        super(code, defaultMessage);
        this.code = code;
    }

    private void setCode(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}


