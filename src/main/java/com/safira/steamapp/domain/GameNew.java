package com.safira.steamapp.domain;

import java.util.Date;

/**
 * Created by developer on 25/09/15.
 */
public class GameNew {

    private String title;
    private String url;
    private String author;
    private String contents;
    private String timestamp;

    public GameNew(String title, String url, String author, String contents, String date) {
        this.title = title;
        this.url = url;
        this.author = author;
        this.contents = contents;
        this.timestamp = date;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getAuthor() {
        return author;
    }

    public String getContents() {
        return contents;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Date getDate() {
        Date date = new Date((Long.parseLong(timestamp))*1000);
        return date;
    }


}
