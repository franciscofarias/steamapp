package com.safira.steamapp.domain;

import com.safira.steamapp.service.HTTPRequester;

import javax.persistence.*;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Comparator;

/**
 * Created by developer on 30/01/15.
 */

@Entity
@Table(name = "GAMES")
public class Game implements Comparable<Game>, Serializable {

    @Id
    @Column(name="GAME_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name="GAME_APP_ID")
    private String appid;

    @Column(name="GAME_NAME")
    private String name;

    @Column(name="GAME_PLAY_TIME")
    private String playTime;

    @Column(name="GAME_ICON_URL")
    private String iconURL;

    @Column(name="GAME_LOGO_URL")
    private String logoURL;

    @ManyToOne
    private Player owner;

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public Player getOwner() {
        return owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPlayTime(String playTime) {
        this.playTime = playTime;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    // Default Constructor
    public Game(){}

    public Game(String appid, String name, String playTime, String iconHash, String logoHash){
        HTTPRequester http = new HTTPRequester();

        this.appid = appid;
        this.name = name;
        this.playTime = playTime;
        this.iconURL = http.getImage(iconHash, appid);
        this.logoURL = http.getImage(logoHash, appid);
    }

    public String getAppid() {
        return appid;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Game o) {
        return 0;
    }

    public static Comparator<Game> GameNameComparator = new Comparator<Game>() {
        public int compare(Game game1, Game game2) {
            String gameName1 = game1.getName().toUpperCase();
            String gameName2 = game2.getName().toUpperCase();

            return gameName1.compareTo(gameName2);
        }
    };

    public String getPlayTime() {
        return playTime;
    }

    public String getIconURL() {
        return iconURL;
    }

    public String getLogoURL() {
        return logoURL;
    }

    public String getPlayedHours() {

        Double hoursPlayed = Double.parseDouble(playTime)/60;
        return new DecimalFormat("#.#").format(hoursPlayed);

    }
}
