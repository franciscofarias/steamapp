package com.safira.steamapp.domain;

/**
 * Created by developer on 22/01/15.
 */
public class SteamUser {
    private String steamID64;
    private String vanity;
    private Player user;

    public SteamUser(String steamID64, String vanity, Player user) {
        this.steamID64 = steamID64;
        this.vanity = vanity;
        this.user = user;
    }

    public String getSteamID64() {
        return steamID64;
    }

    public String getVanity() {
        return vanity;
    }

    public Player getUser() {
        return user;
    }
}
