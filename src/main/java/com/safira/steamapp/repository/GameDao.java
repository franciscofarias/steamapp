package com.safira.steamapp.repository;

import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.Player;

import java.util.List;

/**
 * Created by developer on 02/10/15.
 */
public interface GameDao {

    public List<Game> getGameList();

    public void saveGame(Game game);

    public void addGame(Game game);

    public Game getGameById(long id);

    public List<Game> getGamesByOwner(long playerId);

    public Game getGameByOwnerAndAppId(Player player, String appId);

    public void deleteGame(Game game);

}
