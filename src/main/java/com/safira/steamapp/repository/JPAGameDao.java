package com.safira.steamapp.repository;

import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.Player;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by developer on 02/10/15.
 */
@Repository(value = "gameDao")
public class JPAGameDao implements GameDao {

    private EntityManager em = null;

    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<Game> getGameList() {
        return em.createQuery("SELECT g FROM Game g").getResultList();
    }

    @Transactional(readOnly = false)
    public void saveGame(Game game) {
        em.merge(game);
    }

    @Transactional(readOnly = false)
    public void addGame(Game game) {
        em.persist(game);
    }

    @Transactional(readOnly = true)
    public Game getGameById(long id) {
        return em.find(Game.class, id);
    }

    @Transactional(readOnly = true)
    public List<Game> getGamesByOwner(long playerId) {
        Query query = em.createQuery("SELECT g FROM Game g WHERE g.owner.id = :playerId");
        query.setParameter("playerId", playerId);
        List<Game> resultList = query.getResultList();
        return resultList;
    }

    @Transactional(readOnly = true)
    public Game getGameByOwnerAndAppId(Player player, String appId) {
        Query query = em.createQuery("SELECT g FROM Game g WHERE g.owner.id = :player_id AND g.appid = :app_id");
        query.setParameter("player_id", player.getId());
        query.setParameter("app_id", appId);

        try {
            return (Game) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Transactional(readOnly = false)
    public void deleteGame(Game game) {
        Game gameToDelete = em.getReference(Game.class,game.getId());
        em.remove(gameToDelete);
    }
}
