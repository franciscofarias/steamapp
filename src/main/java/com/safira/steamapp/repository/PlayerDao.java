package com.safira.steamapp.repository;

import com.safira.steamapp.domain.Player;

import java.util.List;

/**
 * Created by developer on 30/09/15.
 */
public interface PlayerDao {

    public List getPlayerList();

    public void savePlayer(Player player);

    public void addPlayer(Player player);

    public Player getPlayerById(long id);

    public Player getPlayerBySteamId(String steamId);

}
