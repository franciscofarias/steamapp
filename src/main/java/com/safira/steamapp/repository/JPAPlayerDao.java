package com.safira.steamapp.repository;

import com.safira.steamapp.domain.Player;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by developer on 30/09/15.
 */
@Repository(value = "playerDao")
public class JPAPlayerDao implements PlayerDao {

    private EntityManager em = null;

    @PersistenceContext
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<Player> getPlayerList() {
        return em.createQuery("SELECT p FROM Player p").getResultList();
    }

    @Transactional(readOnly = false)
    public void savePlayer(Player player) {
        em.merge(player);
    }

    @Transactional(readOnly = false)
    public void addPlayer(Player player) {
        em.persist(player);
    }

    @Transactional(readOnly = true)
    public Player getPlayerById(long id) {
        return em.find(Player.class, id);
    }

    @Transactional(readOnly = true)
    public Player getPlayerBySteamId(String steamId) {
        Query q = em.createQuery("SELECT p FROM Player p WHERE steamID64 = :steam_id");
        q.setParameter("steam_id", steamId);
        try {
            return (Player) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
