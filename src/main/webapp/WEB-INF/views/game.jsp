<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="news_url"  value="/news" />
<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-body panel-default">
          <div class="row">
            <div class="col-sm-12">
              <h1 class="text-center">Game</h1>
            </div>
          </div>
            <div class="row">
              <div class="col-sm-4">
                <p class="text-center">
                  <img class="img img-thumbnail img-responsive" src="${game.logoURL}"/>
                </p>
                <p class="text-center">
                  <a href="${news_url}?appid=${game.appid}" class="btn btn-default">News</a>
                </p>
              </div>
              <div class="col-sm-8">
                <p>
                   <strong>Game Name:</strong> <c:out value="${game.name}"/>
                 </p>
                <p>
                   <strong>Store link: </strong> <a href="http://store.steampowered.com/app/<c:out value='${game.appid}'/>">http://store.steampowered.com/app/<c:out value='${game.appid}'/></a>
                 </p>
                <p>
                   <strong>Steam Application ID:</strong> <c:out value="${game.appid}"/>
                 </p>
                <p>
                  <strong>Hours Played:</strong> <c:out value="${game.playedHours}"/> hs
                </p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>