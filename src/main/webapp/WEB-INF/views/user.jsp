<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="gameList_url"  value="/gameList" />
<c:url var="friendList_url"  value="/friendList" />
<c:url var="userInfo_url"  value="/userInfo" />
<c:url var="recentlyPlayed_url"  value="/recentlyPlayed" />
<c:url var="updateData_url"  value="/updateData" />
<c:url var="quit_url"  value="/quit" />
<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-body panel-default">
          <h1 class="text-center">Menu</h1>
            <spring:hasBindErrors name="errors">
            <c:forEach var="error" items="${errors.errors}">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <c:out value="${error.value}"/>
                            </div>
            </c:forEach>
            </spring:hasBindErrors>

            <div class="main-menu">
                <p class="text-center">
                  <a href="${gameList_url}" class="btn btn-default text-center">Games List</a>
                </p>
                <p class="text-center">
                  <a href="${friendList_url}" class="btn btn-default text-center">Friends</a>
                </p>
                <p class="text-center">
                  <a href="${userInfo_url}" class="btn btn-default text-center">${steamUser.vanity}</a>
                </p>
                <p class="text-center">
                  <a href="${recentlyPlayed_url}" class="btn btn-default text-center">Recently Played</a>
                </p>
                <p class="text-center">
                  <a href="${updateData_url}" class="btn btn-default text-center">Update User Data</a>
                </p>
                <p class="text-center">
                  <a href="${quit_url}" class="btn btn-danger text-center">Quit</a>
                </p>
            </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>