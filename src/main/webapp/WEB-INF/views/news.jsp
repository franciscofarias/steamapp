<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-body">
              <h1 class="text-center">News</h1>
              <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
              <c:forEach var="newItem" items="${news}">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title"><a href="${newItem.url}">${newItem.title}</a></h3>
                    </div>
                    <div class="panel-body">
                      <p>${newItem.contents}</p>
                    </div>
                    <div class="panel-footer">
                      <p><span class="pull-left"><fmt:formatDate value="${newItem.date}" pattern="yyyy-MM-dd"/></span>
                      <span class="pull-right"><em>by: </em>${newItem.author}</span></p>
                    </div>
                  </div>
                </div>
              </c:forEach>

              <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>

            </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>