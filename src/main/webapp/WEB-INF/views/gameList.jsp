<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="game_url"  value="/game" />
<c:url var="home_url"  value="/home" />
<c:url var="updateGameData_url"  value="/updateGameData" />
<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-body panel-default">
          <h1 class="text-center">These are your games</h1>
            <p class="text-center"><a class="btn btn-default" href="${home_url}">Back</a></p>
            <p class="text-center"><a class="btn btn-default" href="${updateGameData_url}">Update Game Data</a></p>
            <table class="table table-hover">
              <tr><th>Game</th></tr>
              <c:forEach var="game" items="${gameList}">
                <tr><td>
                  <a href="${game_url}?appid=${game.appid}">${game.name}</a>
                </td></tr>
              </c:forEach>
          </table>
          <p class="text-center"><a class="btn btn-default" href="${home_url}">Back</a></p>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>