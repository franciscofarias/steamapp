<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
    <div class="container">
    	<div class="row">
    		<div class="col-md-6 col-md-offset-3">
    			<div class="panel panel-body panel-default">
    				<h1 class="text-center">Please insert your username</h1>
    				
    				<spring:hasBindErrors name="vanityNameForm">
    				        <c:forEach var="error" items="${errors.allErrors}">
    				           <div class="alert alert-danger alert-dismissible" role="alert">
    				                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    				           		<strong>Oops! </strong><spring:message code="${error.code}"/>
    				           </div>
    				        </c:forEach>
    				</spring:hasBindErrors>
    				
    				<c:url var="home_url" value="/home" />
    				<form:form action="${home_url}" method="POST" modelAttribute="vanityNameForm">
    				    <div class="form-group">
    				        <form:label path="Nickname">Nickname</form:label>
    				        <div class="input-group">
                                <form:input path="Nickname" cssClass="form-control" autofocus="autofocus"></form:input>
                                <span class="input-group-btn">
                                    <input type="submit" value="Submit" class="btn btn-primary"/>
                                </span>
                            </div>
                        </div>
    				</form:form>
    			</div>
    		</div>
    	</div>
    </div>
  </body>
</html>