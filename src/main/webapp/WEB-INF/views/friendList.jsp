<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="friend_url"  value="/friend" />
<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-body">
            <h1 class="text-center">These are your friends</h1>
            <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
              <table class="table table-hover">
              <c:forEach var="friend" items="${friends}">
              <tr><td>
              <a href="${friend_url}?steamID64=${friend.steamID64}">${friend.personalName}
              </td></tr>
              </c:forEach>
              </table>
              <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>