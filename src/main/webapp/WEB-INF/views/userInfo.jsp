<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-12">
                <h1 class="text-center">Profile</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
                <p>
                  <img class="img img-thumbnail img-responsive" src="${steamUser.user.avatarFullURL}"/>
                </p>
              </div>
              <div class="col-sm-8">
                <p><strong>Steam ID 64 bit:</strong>  ${steamUser.user.steamID64}</p>
                <p><strong>Nick name:</strong>  ${steamUser.user.personalName}</p>
                <p><strong>Real name:</strong>  ${steamUser.user.realName}</p>
                <p class="text-center">
                  <a class="btn btn-default" href="${steamUser.user.profileURL}">Go to Profile</a>
                </p>
              </div>
            </div>
             <div class="row">
               <div class="col-sm-12">
                  <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
               </div>
             </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>