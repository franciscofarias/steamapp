<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url var="game_url"  value="/game" />
<html>
  <head>
    <%@ include file="/WEB-INF/views/includeHead.jsp" %>
  </head>
  <body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-body panel-default">
          <h1 class="text-center">Recently Played Games</h1>
            <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
            <table class="table table-hover">
              <tr><th>Game</th></tr>
              <c:forEach var="game" items="${games}">
                <tr><td>
                  <a href="${game_url}?appid=${game.appid}">${game.name}</a>
                </td></tr>
              </c:forEach>
          </table>
          <p class="text-center"><button class="btn btn-default" onclick="window.history.back()">Back</button></p>
        </div>
      </div>
    </div>
  </div>
  </body>
</html>