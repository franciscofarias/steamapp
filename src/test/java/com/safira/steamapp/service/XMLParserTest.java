package com.safira.steamapp.service;

import com.safira.steamapp.domain.Game;
import com.safira.steamapp.domain.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by developer on 22/01/15.
 */


public class XMLParserTest {

    private XMLParser xml;
    private static final String VANITY_URL_RESPONSE = "<response>\n" +
            "<steamid>76561198046826019</steamid>\n" +
            "<success>1</success>\n" +
            "</response>";
    private static final String GET_OWNED_GAMES_RESPONSE = "<response>\n" +
            "<game_count>1</game_count>" +
            "<games>" +
            "<message>" +
            "<appid>220</appid>" +
            "<name>Half-Life 2</name>" +
            "<playtime_forever>353</playtime_forever>" +
            "<img_icon_url>fcfb366051782b8ebf2aa297f3b746395858cb62</img_icon_url>" +
            "<img_logo_url>e4ad9cf1b7dc8475c1118625daf9abd4bdcbcad0</img_logo_url>" +
            "<has_community_visible_stats>true</has_community_visible_stats>" +
            "</message>" +
            "</games>" +
            "</response>";
    private static final String GET_FRIEND_LIST_RESPONSE = "<friendslist>" +
            "<friends>" +
            "<friend>" +
            "<steamid>76561197960304530</steamid>" +
            "<relationship>friend</relationship>" +
            "<friend_since>1359491225</friend_since>" +
            "</friend>" +
            "</friends>" +
            "</friendslist>";

    private static final String GET_PLAYER_SUMMARIES_RESPONSE = "<response>" +
            "<players>" +
            "<player>" +
            "<steamid>76561198046826019</steamid>" +
            "<communityvisibilitystate>3</communityvisibilitystate>" +
            "<profilestate>1</profilestate>" +
            "<personaname>FractalSage</personaname>" +
            "<lastlogoff>1422586412</lastlogoff>" +
            "<profileurl>http://steamcommunity.com/id/FractalSage/</profileurl>" +
            "<avatar>http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f4/f4454854707f4c679d9f48c1fb1a566f694aa9d2.jpg</avatar>" +
            "<avatarmedium>http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f4/f4454854707f4c679d9f48c1fb1a566f694aa9d2_medium.jpg</avatarmedium>" +
            "<avatarfull>http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f4/f4454854707f4c679d9f48c1fb1a566f694aa9d2_full.jpg</avatarfull>" +
            "<personastate>0</personastate>" +
            "<realname>Francisco Farías</realname>" +
            "<primaryclanid>103582791432720446</primaryclanid>" +
            "<timecreated>1313249507</timecreated>" +
            "<personastateflags>0</personastateflags>" +
            "<loccountrycode>AR</loccountrycode>" +
            "<locstatecode>06</locstatecode>" +
            "<loccityid>5540</loccityid>" +
            "</player>" +
            "</players>" +
            "</response>";



    @Before
    public void startUp() {
        xml = new XMLParser();
    }

    @Test
    public void shouldReturnSteamID64() throws Exception {
        String result = xml.getSteamID64(VANITY_URL_RESPONSE);
        assertEquals("76561198046826019", result);
    }

    @Test
    public void shouldReturnAMapWithGame() throws Exception {
        List<Game> result = xml.retrieveGames(GET_OWNED_GAMES_RESPONSE);
        Game game = new Game("220", "Half-Life 2", "353", "fcfb366051782b8ebf2aa297f3b746395858cb62", "e4ad9cf1b7dc8475c1118625daf9abd4bdcbcad0");
        assertEquals(game.getName(), result.get(0).getName());
        assertEquals(game.getAppid(), result.get(0).getAppid());
        assertEquals(game.getIconURL(), result.get(0).getIconURL());
        assertEquals(game.getLogoURL(), result.get(0).getLogoURL());
        assertEquals(game.getPlayTime(), result.get(0).getPlayTime());
    }

    @Test
    public void shouldReturnFriendList() throws Exception {
        String result = xml.retrieveFriendList(GET_FRIEND_LIST_RESPONSE);
        assertEquals("76561197960304530", result);
    }

    @Test
    public void ShouldReturnPlayerSummaries() throws Exception {
        List<Player> result = xml.retrievePlayerSummaries(GET_PLAYER_SUMMARIES_RESPONSE);
        Player player = new Player("76561198046826019",
                "FractalSage",
                "http://steamcommunity.com/id/FractalSage/",
                "http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f4/f4454854707f4c679d9f48c1fb1a566f694aa9d2_full.jpg",
                "Francisco Farías");
        assertEquals(player.getAvatarFullURL(),result.get(0).getAvatarFullURL());
        assertEquals(player.getPersonalName(),result.get(0).getPersonalName());
        assertEquals(player.getProfileURL(),result.get(0).getProfileURL());
        assertEquals(player.getRealName(),result.get(0).getRealName());
        assertEquals(player.getSteamID64(),result.get(0).getSteamID64());
    }

    @Test
    public void ShouldReturnPlayerSummary() throws Exception {
        Player result = xml.retrieveUserSummary(GET_PLAYER_SUMMARIES_RESPONSE);
        Player player = new Player("76561198046826019",
                "FractalSage",
                "http://steamcommunity.com/id/FractalSage/",
                "http://cdn.akamai.steamstatic.com/steamcommunity/public/images/avatars/f4/f4454854707f4c679d9f48c1fb1a566f694aa9d2_full.jpg",
                "Francisco Farías");
        assertEquals(player.getAvatarFullURL(),result.getAvatarFullURL());
        assertEquals(player.getPersonalName(),result.getPersonalName());
        assertEquals(player.getProfileURL(),result.getProfileURL());
        assertEquals(player.getRealName(),result.getRealName());
        assertEquals(player.getSteamID64(),result.getSteamID64());
    }
}
